/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * http://www.cplusplus.com/reference/cstdio/fread/
 * http://www.cplusplus.com/reference/cstdio/fwrite/
 * http://stackoverflow.com/questions/17663186/initializing-a-two-dimensional-stdvector
 * http://stackoverflow.com/questions/11821491/converting-string-to-cstring-in-c
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:4
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <iostream>
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

vector<vector<bool>> grid;
char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	mainLoop();
	return 0;
}

void mainLoop() {
	/* update, write, sleep */
/*	grid = {{1,0,1},
			{0,0,0},
			{0,0,1}};
			*/
	initFromFile(initfilename);
	FILE* f;
	if(wfilename == "-")f = stdout;
	else f = fopen(wfilename.c_str(), "wb");
	if(max_gen > 0){
		while(max_gen > 0){
			update();
			max_gen--;
		}
		dumpState(f);
	}
	else{
		while(true){
			update();
			dumpState(f);
			rewind(f);
			sleep(1);
		}
	}
	fclose(f);
}
void dumpState(FILE* f){
	char c;
	/* go through array and write each true/false as ./0 */
	for(size_t i = 0; i < grid.size(); i++){
		for(size_t j = 0; j < grid[i].size(); j++){
			c = text[grid[i][j]];
			fwrite(&c, 1, 1, f);
		}
		c = '\n';
		fwrite(&c, 1, 1, f);
	}
}

void update() {
	//Declaring sizes
	int i = grid.size();
	int j = grid[0].size();
	//Initializing new vector of the same size
	vector<vector<bool>> newGrid(i,vector<bool>(j, false));
	size_t neighbors;	
	//Parsing through the 2D vector
	for (int x=0; x<i; x++) {
		for(int y=0; y<j; y++){
			neighbors = nbrCount(x,y,grid);
			//if more than 3 or less than 2, they die
			if (neighbors > 3 || neighbors < 2){
				newGrid[x][y] = false;
			}
	
			//if has 3 neighbors or is alive with 2, come to life
			if (neighbors == 3 || (grid[x][y] && neighbors == 2)){
				newGrid[x][y] = true;
			}
		}
	}
	//Updating new vector with old vector
	for (int x=0; x<i; x++) {
		for(int y=0; y<j; y++){	
			grid[x][y] = newGrid[x][y];
		}
	}	
}
int initFromFile(const string& fname) {
	FILE* f = fopen(fname.c_str(), "rb"); // opens file for reading
	if(fname == "-")f=stdin;
	if (!f) {
		exit(1);
	}
	grid.push_back(vector<bool>());
	char c;
	size_t i = 0;
	while(fread(&c, sizeof c, 1, f)){
		if(c=='.')
			grid[i].push_back(false);
		else if(c=='O')
			grid[i].push_back(true);
		else if(c=='\n'){
			grid.push_back(vector<bool>());
			i++;
		}
	}
	grid.erase(grid.begin() + i);
	fclose(f);
	return 1;
}

size_t nbrCount(size_t i,size_t j,const vector<vector<bool> > & g){
	size_t count = 0;
	int rowInd = 0;//The indexes I will check
	int columnInd = 0;
	for(int x=0;x<3;x++){
		for(int y=0;y<3;y++){
			if(x == 1 && y == 1){
				continue;
			}
			rowInd = (int)i-1+x;
			columnInd = (int)j-1+y;
			if((int)i-1+x == -1){
				rowInd = g.size()-1;
			}
			if((int)j-1+y == -1){
				columnInd = g[i].size()-1;
			}
			if(i-1+x == g.size()){
				rowInd = 0;
			}
			if(j-1+y == g[i].size()){
				columnInd = 0;
			}
			if(g[rowInd][columnInd]){
				count++;
			}
		}
	}	
	return count;//returns the amount of neighbors
}
